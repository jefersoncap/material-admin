import Vue from 'vue'
import VueRouter from 'vue-router'
import App from './App.vue'
import vuetify from './plugins/vuetify';
import CompanyList from './components/company/CompanyList';
import CompanyDetails from './components/company/CompanyDetails';

Vue.config.productionTip = false

Vue.use(VueRouter)

const routes = [
  { path: '/company', component: CompanyList },
  { path: '/company/:id', component: CompanyDetails }
]

const router = new VueRouter({
  routes
})

new Vue({
  vuetify,
  router,
  render: h => h(App)
}).$mount('#app')
